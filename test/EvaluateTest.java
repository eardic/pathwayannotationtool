/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS-PC
 */
public class EvaluateTest {

    public EvaluateTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private void createData(List<Protein> predictions, List<Protein> testProteinList) {
        // No pathway data
        predictions.add(new Protein("Q12345"));
        testProteinList.add(new Protein("Q12345"));
        // Some Pathway data
        Protein p1 = new Protein("Q54321");
        p1.getPathwayList().add("1111");
        p1.getPathwayList().add("2222");
        p1.getPathwayList().add("3333");
        testProteinList.add(p1);

        Protein p2 = new Protein("Q54321");
        p2.getPathwayList().add("0000");
        p2.getPathwayList().add("4444");
        p2.getPathwayList().add("2222");
        p2.getPathwayList().add("3333");
        predictions.add(p2);
    }

    /**
     * Test of evaluate method, of class Evaluate.
     */
    @Test
    public void testEvaluate() {
        System.out.println("evaluate");
        List<Protein> predictions, testProteinList;
        predictions = new ArrayList<Protein>();
        testProteinList = new ArrayList<Protein>();

        createData(predictions, testProteinList);
        
        assertTrue(Evaluate.intersectCount(testProteinList.get(0), predictions.get(0)) == 0);
        assertTrue(Evaluate.intersectCount(testProteinList.get(1), predictions.get(1)) == 2);

        Evaluate instance = new Evaluate(predictions, testProteinList);
        instance.evaluate();

        assertTrue(instance.getMeasure(Evaluate.Metric.RECALL) > 0);
        assertTrue(instance.getMeasure(Evaluate.Metric.PRECISION) > 0);
        assertTrue(instance.getMeasure(Evaluate.Metric.FVALUE) > 0);

    }

}
