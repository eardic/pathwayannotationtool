/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS-PC
 */
public class DataParserTest {

    public DataParserTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of parseProteins method, of class DataParser.
     */
    @Test
    public void testParseProteins() {
        System.out.println("parseProteins");
        File file = new File("C:\\Users\\Emre\\Documents\\TestPathways.csv");
        List<Protein> result = DataParser.parseProteins(file);
        // First element
        assertTrue(result.get(0).getUniprot().equals("A0JLT2"));
        assertTrue(result.get(0).getPathwayList().contains("485243"));
        // Last element
        assertTrue(result.contains(new Protein("O95239")));
        assertTrue(result.get(result.indexOf(new Protein("O95239"))).getPathwayList().contains("485342"));
    }

    /**
     * Test of parseInteractions method, of class DataParser.
     */
    @Test
    public void testParseInteractions() {
        System.out.println("parseInteractions");
        File file = new File("C:\\Users\\Emre\\Documents\\TestHPPI.csv");
        List<PPI> result = DataParser.parseInteractions(file);
        assertTrue(result.get(0).p1.getUniprot().equals("O95402"));
        assertTrue(result.get(0).p2.getUniprot().equals("A0JLT2"));
        assertTrue(result.get(result.size() - 1).p1.getUniprot().equals("P06239"));
        assertTrue(result.get(result.size() - 1).p2.getUniprot().equals("P10721"));
    }

    /**
     * Test of parseInteractions method, of class DataParser.
     */
    @Test
    public void testFilterProteins() {
        System.out.println("parseInteractions");
        File ppiFile = new File("C:\\Users\\ASUS-PC\\Documents\\TrainHPPI.csv");
        File pFile = new File("C:\\Users\\ASUS-PC\\Documents\\TrainPathways.csv");
        List<Protein> proteins = DataParser.parseProteins(pFile);
        List<PPI> ppiList = DataParser.parseInteractions(ppiFile);

        System.err.println("Loaded proteins size: " + proteins.size());
        System.err.println("Loaded ppi size: " + ppiList.size());
        DataParser.filterProteins(ppiList, proteins);

        // All proteins in ppiList must also available in proteins
        for (PPI p : ppiList) {
            //assertFalse(p.p1.getPathwayList().isEmpty());
            //assertFalse(p.p2.getPathwayList().isEmpty());
            assertTrue(proteins.indexOf(p.p1) != -1);
            assertTrue(proteins.indexOf(p.p2) != -1);
        }
        System.err.println("Filtered ppi size: " + ppiList.size());
        // All proteins must be in ppiList
        for (Protein protein : proteins) {
            boolean remove = true;
            for (PPI ppi : ppiList) {
                if (protein.equals(ppi.p1) || protein.equals(ppi.p2)) {
                    remove = false;
                    break;//Next protein
                }
            }
            assertFalse(remove);
        }
        System.err.println("Filtered proteins size: " + proteins.size());
    }

}
