/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.text.AttributedCharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

/**
 *
 * @author ASUS-PC
 */
public class FeatureBasedTest {

    public FeatureBasedTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of predict method, of class FeatureBased.
     */
    @Test
    public void testPredict() {
        System.out.println("predict");
        List<Protein> testPathway, trainPathway;
        List<PPI> trainPPI;
        testPathway = new ArrayList<Protein>();
        trainPathway = new ArrayList<Protein>();
        trainPPI = new ArrayList<PPI>();
        //Small test data
        testPathway.add(new Protein("Q12345"));
        testPathway.get(0).getPathwayList().add("34455");
        testPathway.get(0).getPathwayList().add("34623");
        testPathway.add(new Protein("Q33333"));
        testPathway.get(1).getPathwayList().add("22222");
        testPathway.get(1).getPathwayList().add("33333");
        testPathway.get(1).getPathwayList().add("44444");

        // Small train data
        trainPathway.add(new Protein("Q11111"));
        trainPathway.get(0).getPathwayList().add("22222");
        trainPathway.get(0).getPathwayList().add("33333");
        trainPathway.get(0).getPathwayList().add("44444");
        trainPathway.add(new Protein("Q00000"));
        trainPathway.get(1).getPathwayList().add("34455");
        trainPathway.get(1).getPathwayList().add("34623");
        trainPathway.get(1).getPathwayList().add("12363");
        // Small ppi data
        trainPPI.add(new PPI(new Protein("Q00000"), new Protein("Q12345")));
        trainPPI.add(new PPI(new Protein("Q33333"), new Protein("Q11111")));
        trainPPI.add(new PPI(new Protein("Q00000"), new Protein("Q11111")));
        FeatureBased fba = new FeatureBased(trainPPI, testPathway, trainPathway);

        FastVector attrList = fba.createAttributeList();
        assertTrue(attrList.contains(new Attribute("22222")));
        assertTrue(attrList.contains(new Attribute("33333")));
        assertTrue(attrList.contains(new Attribute("44444")));
        assertTrue(attrList.contains(new Attribute("34455")));
        assertTrue(attrList.contains(new Attribute("34623")));
        assertTrue(attrList.contains(new Attribute("12363")));
        System.out.println(attrList.indexOf(new Attribute("12363")));
        assertTrue(attrList.indexOf(new Attribute("12363")) != -1);
        assertTrue(attrList.size() == 6);

        Instance inst = new SparseInstance(attrList.size());
        fba.createInstance(inst, attrList, trainPathway.get(0), null);
        assertTrue(inst.numAttributes() == 6);

        Instances trainInstances = new Instances("TrainInsts", attrList, trainPathway.size());
        fba.createInstances(trainInstances, attrList, trainPathway);
        assertTrue(trainInstances.numInstances() == trainPathway.size());

    }

}
