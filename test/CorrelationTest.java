/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ASUS-PC
 */
public class CorrelationTest {

    public CorrelationTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private void prepareData(List<Protein> testPathway, List<Protein> proteinDataSet, List<PPI> trainPPI) {
        testPathway.add(new Protein("A"));
        testPathway.get(0).getPathwayList().add("p1");
        testPathway.get(0).getPathwayList().add("p2");
        testPathway.add(new Protein("B"));
        testPathway.get(1).getPathwayList().add("p3");
        testPathway.get(1).getPathwayList().add("p4");
        testPathway.get(1).getPathwayList().add("p5");

        // Small protein data
        proteinDataSet.add(new Protein("D"));
        proteinDataSet.get(0).getPathwayList().add("p3");
        proteinDataSet.get(0).getPathwayList().add("p6");
        proteinDataSet.get(0).getPathwayList().add("p7");
        proteinDataSet.add(new Protein("C"));
        proteinDataSet.get(1).getPathwayList().add("p1");
        proteinDataSet.get(1).getPathwayList().add("p2");
        proteinDataSet.get(1).getPathwayList().add("p8");
        proteinDataSet.add(new Protein("A"));
        proteinDataSet.get(2).getPathwayList().add("p1");
        proteinDataSet.get(2).getPathwayList().add("p2");
        proteinDataSet.add(new Protein("B"));
        proteinDataSet.get(3).getPathwayList().add("p3");
        proteinDataSet.get(3).getPathwayList().add("p4");
        proteinDataSet.get(3).getPathwayList().add("p5");
        // Small ppi data
        trainPPI.add(new PPI(new Protein("C"), new Protein("A")));
        trainPPI.add(new PPI(new Protein("C"), new Protein("B")));
        trainPPI.add(new PPI(new Protein("B"), new Protein("D")));
        trainPPI.add(new PPI(new Protein("C"), new Protein("D")));
    }

    /**
     * Test of predict method, of class Correlation.
     */
    @Test
    public void testPredict() {
        System.out.println("predict");
        List<Protein> testPathway, proteinDataSet;
        List<PPI> trainPPI;
        testPathway = new ArrayList<Protein>();
        proteinDataSet = new ArrayList<Protein>();
        trainPPI = new ArrayList<PPI>();

        prepareData(testPathway, proteinDataSet, trainPPI);

        Correlation instance = new Correlation(trainPPI, testPathway, proteinDataSet, 3);
        List<Protein> result = instance.predict();
        assertTrue(result.get(0).getPathwayList().size() == 3);
        assertTrue(result.get(1).getPathwayList().size() == 3);
        assertTrue(result.get(0).equals(new Protein("A")));
        assertTrue(result.get(1).equals(new Protein("B")));
    }
}
