
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ASUS-PC
 */
public class DataParser {

    /**
     * Fills pathways of proteins in ppi using given "proteins" list
     *
     * @param ppiList
     * @param proteins
     */
    public static void filterProteins(List<PPI> ppiList, List<Protein> proteins) {
        List<PPI> newPPIList = new LinkedList<PPI>();
        for (PPI ppi : ppiList) {
            int p1Idx = proteins.indexOf(ppi.p1);
            int p2Idx = proteins.indexOf(ppi.p2);
            // If proteins not in proteins list, then don't add it, incomplete data not permitted
            if (p1Idx != -1 && p2Idx != -1) {
                // Get set of patway of p1 from proteins and assign the pathways to p1
                //ppi.p1.getPathwayList().addAll(proteins.get(p1Idx).getPathwayList());
                // ppi.p2.getPathwayList().addAll(proteins.get(p2Idx).getPathwayList());
                newPPIList.add(ppi);
            }
        }
        ppiList.clear();
        ppiList.addAll(newPPIList);
        List<Protein> newProteins = new LinkedList<Protein>();
        for (Protein protein : proteins) {
            for (PPI ppi : ppiList) {
                if (protein.equals(ppi.p1) || protein.equals(ppi.p2)) {
                    newProteins.add(protein);
                    break;//Next protein
                }
            }
        }
        proteins.clear();
        proteins.addAll(newProteins);
        System.err.println("Filtered ppi size: " + ppiList.size());
        System.err.println("Filtered protein size: " + proteins.size());
    }

    public static List<PPI> mergeData(List<PPI> ppiList, List<Protein> proteins) {
        List<PPI> merged = new ArrayList<PPI>();
        for (PPI ppi : ppiList) {
            int p1Idx = proteins.indexOf(ppi.p1);
            int p2Idx = proteins.indexOf(ppi.p2);
            // If proteins not in proteins list, then don't add it, incomplete data not permitted
            if (p1Idx != -1 && p2Idx != -1) {
                ppi.p1.setPathwayList(proteins.get(p1Idx).getPathwayList());
                ppi.p2.setPathwayList(proteins.get(p2Idx).getPathwayList());
                merged.add(ppi);
            }
        }
        System.err.println("Merged ppi size: " + merged.size());
        return merged;
    }

    public static List<Protein> getProteinSet(List<PPI> ppiList) {
        Set<Protein> protSet = new HashSet<Protein>();
        for (PPI ppi : ppiList) {
            protSet.add(ppi.p1);
            protSet.add(ppi.p2);

        }
        return new ArrayList<Protein>(protSet);
    }

    public static List<Protein> parseProteins(File file) {
        BufferedReader reader = null;
        try {
            List<Protein> proteinList = new LinkedList<Protein>();
            reader = new BufferedReader(new FileReader(file));
            reader.readLine();// Skip header
            while (reader.ready()) {
                String line = reader.readLine();
                String[] data = line.split(",");
                Protein proteinRead = new Protein(data[0]);
                //System.out.println(data[0] + "," + data[1]);
                if (proteinList.contains(proteinRead)) {
                    //Get protein and update its pathway list
                    proteinList.get(proteinList.indexOf(proteinRead)).getPathwayList().add(data[1]);
                } else {
                    //Add new protein to list
                    proteinRead.getPathwayList().add(data[1]);
                    proteinList.add(proteinRead);
                }
            }
            return proteinList;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public static List<PPI> parseInteractions(File file) {
        BufferedReader reader = null;
        try {
            List<PPI> ppi = new LinkedList<PPI>();
            reader = new BufferedReader(new FileReader(file));
            reader.readLine();// Skip header
            while (reader.ready()) {
                String line = reader.readLine();
                //System.out.println(line);
                String[] uniprots = line.split(",");
                ppi.add(new PPI(new Protein(uniprots[0]), new Protein(uniprots[1])));
            }
            return ppi;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(DataParser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }
}
