/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS-PC
 */
public class PPI {

    Protein p1, p2;

    public PPI() {
        this(null, null);
    }

    public PPI(Protein p1, Protein p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Protein getP1() {
        return p1;
    }

    public void setP1(Protein p1) {
        this.p1 = p1;
    }

    public Protein getP2() {
        return p2;
    }

    public void setP2(Protein p2) {
        this.p2 = p2;
    }

    public boolean equals(PPI obj) {
        return obj.p1.equals(obj.p1) && obj.p2.equals(obj.p2);
    }

    @Override
    public String toString() {
        return p1.getUniprot() + "-" + p2.getUniprot();
    }

}
