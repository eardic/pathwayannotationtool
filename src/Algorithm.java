
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ASUS-PC
 */
public abstract class Algorithm {

    protected List<PPI> ppiList;
    protected List<Protein> testProteinList, trainProteinList;

    public Algorithm(List<PPI> ppiList, List<Protein> testProteinList, List<Protein> trainProteinList) {
        this.ppiList = ppiList;
        this.testProteinList = testProteinList;
        this.trainProteinList = trainProteinList;
    }
    
    public abstract List<Protein> predict();    
}
