
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ASUS-PC
 */
public class Evaluate {

    public enum Metric {

        RECALL, PRECISION, FVALUE, ACCURACY;
    }
    private List<Protein> predictions, testProteinList;
    Map<Metric, Double> measure;

    public Evaluate(List<Protein> predictions, List<Protein> testProteinList) {
        this.predictions = predictions;
        this.testProteinList = testProteinList;
        this.measure = new HashMap<Metric, Double>();
    }

    public double getMeasure(Metric m) {
        return measure.get(m);
    }

    public static int intersectCount(Protein testP, Protein predictP) {
        int intersection = 0;
        for (String testPathway : testP.getPathwayList()) {
            if (predictP.getPathwayList().contains(testPathway)) {
                ++intersection;
            }
        }
        return intersection;
    }

    public static int unionCount(Protein testP, Protein predictP) {
        Set<String> union = new HashSet<String>();
        union.addAll(testP.getPathwayList());
        union.addAll(predictP.getPathwayList());
        return union.size();
    }

    public void evaluate() {
        double avgPrecision = 0, avgRecall = 0, avgFValue = 0, avgAcc = 0;
        int calcNum = testProteinList.size();
        for (Protein testP : testProteinList) {
            int predictPIdx = predictions.indexOf(testP);
            if (predictPIdx != -1) {
                Protein predictP = predictions.get(predictPIdx);
                double intersection = intersectCount(testP, predictP);
                double union = unionCount(testP, predictP);//For accuracy
                int tSize = testP.getPathwayList().size();
                if (tSize <= 0) {// Test connot be empty, unexcepted situation
                    --calcNum; // Decrement one from test size
                    continue;//Skip
                }
                // |Q INTERSECT R| / |Q| ; Q = prediction set, R = real set
                int pSize = predictP.getPathwayList().size();
                double precision = 0;// If predict set is empty then precision is zero
                if (pSize > 0) {
                    precision = (intersection / pSize);
                } else {
                    System.err.println(predictP.getUniprot() + " Predict set's empty");
                }
                double recall = (intersection / tSize);
                double fValue = ((2 * precision * recall) / (precision + recall));
                avgPrecision += precision;
                avgRecall += recall;
                avgAcc += intersection / union;
                if (precision > 0 && recall > 0) {
                    avgFValue += fValue;
                }
                //System.out.println("TestP: " + testP + " ,I : " + intersection + " , Prec : " + precision + ", Rec:" + recall);
            }
        }
        avgPrecision /= calcNum;
        avgRecall /= calcNum;
        avgFValue /= calcNum;
        avgAcc /= calcNum;
        measure.put(Metric.ACCURACY, avgAcc);
        measure.put(Metric.FVALUE, avgFValue);
        measure.put(Metric.RECALL, avgRecall);
        measure.put(Metric.PRECISION, avgPrecision);
        System.err.println("TestSize: " + testProteinList.size() + ", Calc : " + calcNum
                + ", AvgPrec : " + avgPrecision + ", AvgRec : " + avgRecall + ", AvgF : " + avgFValue + ", Acc:" + avgAcc);
    }
}
