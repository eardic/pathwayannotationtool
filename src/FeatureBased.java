
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JProgressBar;
import weka.classifiers.Classifier;
import weka.classifiers.evaluation.NominalPrediction;
import weka.classifiers.lazy.IBk;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author Emre Ardıç
 */
public class FeatureBased extends Algorithm {

    private Classifier classifier = new IBk(10);
    private JProgressBar pb = new JProgressBar(0, 100);
    private final String ONE_LENGTH_N_ID = "_L1";
    private FastVector classValues = new FastVector();
    private final int POSITIVE = 1, NEGATIVE = 0;
    private FastVector predictions = new FastVector();

    public FeatureBased(List<PPI> ppiList, List<Protein> testProteinList, List<Protein> trainProteinList) {
        super(ppiList, testProteinList, trainProteinList);
        classValues.addElement("0");//Index 0
        classValues.addElement("1");// Index 1
    }

    public void setPb(JProgressBar pb) {
        this.pb = pb;
    }

    public JProgressBar getPb() {
        return pb;
    }

    public void setClassifier(Classifier classifier) {
        this.classifier = classifier;
    }

    public Classifier getClassifier() {
        return classifier;
    }

    /**
     * Creates attribute list using pathways in train set
     *
     * @return attribute list
     */
    public FastVector createAttributeList() {
        // Get unique pathways
        Set<String> pathwaySet = new HashSet<String>();
        for (Protein prot : trainProteinList) {
            for (String path : prot.getPathwayList()) {
                pathwaySet.add(path);
            }
        }
        for (Protein prot : testProteinList) {
            for (String path : prot.getPathwayList()) {
                pathwaySet.add(path);
            }
        }
        // Create feature list, define all pathways as features
        FastVector attributeList = new FastVector(pathwaySet.size());
        // Create nominal attribute list for proteins
        for (String path : pathwaySet) {
            attributeList.addElement(new Attribute(path, this.classValues));
        }
        // Create numeric attribute list for 1-length neighbors of proteins
        for (String path : pathwaySet) {
            attributeList.addElement(new Attribute(path + ONE_LENGTH_N_ID));//Numeric
        }
        System.err.println("Attr List Created!");
        return attributeList;
    }

    /**
     * Creates an instance for given protein by processing trainPPI and
     * trainPathways
     *
     * @param inst returned
     * @param attributeList list of pathways actually
     * @param protein, a protein instance
     * @param outPathway the name of attribute which will be deleted from
     * instance
     */
    public void createInstance(Instance inst, FastVector attributeList, Protein protein, String outPathway) {
        Set<Protein> protSetForTestP = new HashSet<Protein>();
        // Create set of proteins interacting with trainP
        for (PPI ppi : ppiList) {
            if (ppi.p1.equals(protein)) {//If trainP is p1 then add p2
                protSetForTestP.add(ppi.p2);
            } else if (ppi.p2.equals(protein)) {//else add p1
                protSetForTestP.add(ppi.p1);
            }
        }
        // Create pathway list using protein set of given "protein"
        Map<String, Integer> pathwaysForTestP = new HashMap<String, Integer>();
        // Pathways for Neighbors
        for (Protein setP : protSetForTestP) {
            //For neighbors
            for (String path : setP.getPathwayList()) {
                Integer freq = pathwaysForTestP.get(path);
                pathwaysForTestP.put(path, ((freq != null) ? (freq + 1) : 1));
            }
        }
        // Create feature vector for 1-length neigbors of protein
        for (String path : pathwaysForTestP.keySet()) {
            int idx = attributeList.indexOf(new Attribute(path + ONE_LENGTH_N_ID));
            if (idx != -1) {
                Attribute attr = (Attribute) attributeList.elementAt(idx);
                Integer freq = pathwaysForTestP.get(path);
                int value = ((freq != null) ? freq : 0);
                inst.setValue(attr, value);
                //System.out.println("Attr name:" + attr.name() + ", pathway :" + path);
            } else {
                System.err.println("No such attribute error creating instance for " + protein);
            }
        }
        // Create nominal exits(1) or not(0) feature vector for protein
        int size = attributeList.size();
        for (int i = 0; i < size / 2; ++i) {
            Attribute attr = (Attribute) attributeList.elementAt(i);
            if (protein.getPathwayList().contains(attr.name())) {
                inst.setValue(attr, "1");
            } else {
                inst.setValue(attr, "0");
            }
        }
    }

    /**
     * Creates instances for train proteins
     *
     * @param instances output object
     * @param attributeList, pathway list
     * @param data , protein list
     */
    public void createInstances(Instances instances, FastVector attributeList,
            List<Protein> data) {
        for (Protein dataP : data) {
            // Create feature vector
            Instance inst = new Instance(attributeList.size());
            createInstance(inst, attributeList, dataP, null);
            instances.add(inst);
        }
        System.err.println("Feature List Created!");
    }

    /**
     * Predicts pathways of test data and returns test proteins along with their
     * predicted pathways
     *
     * @return list of proteins whose pathways're predicted
     */
    @Override
    public List<Protein> predict() {
        try {
            System.err.println("Using :" + classifier.toString());
            // Pathways are attributes
            FastVector attributeList = createAttributeList();
            // Train instaces for classifiers in WEKA
            Instances trainInstances = new Instances("TrainInsts", attributeList, trainProteinList.size());
            // Create Train set using proteins in train data
            createInstances(trainInstances, attributeList, trainProteinList);
            // Classify test data
            Instances testData = new Instances("Test", attributeList, 1);
            List<Protein> predictList = new ArrayList< Protein>();
            final int TEST_SIZE = testProteinList.size();
            for (int i = 0; i < TEST_SIZE; ++i) {
                Protein testP = testProteinList.get(i);
                Protein predP = new Protein(testP.getUniprot());
                for (String pathway : testP.getPathwayList()) {
                    // Get attribute(pathway) and build classifier
                    int classIdx = attributeList.indexOf(new Attribute(pathway, classValues));
                    // Set class index to pathway
                    trainInstances.setClassIndex(classIdx);
                    classifier.buildClassifier(trainInstances);
                    // Create test instance
                    Instance testInst = new Instance(attributeList.size());
                    testInst.setDataset(testData);
                    testData.setClassIndex(classIdx);
                    createInstance(testInst, attributeList, testP, pathway);
                    // Get prediction
                    double[] dist = classifier.distributionForInstance(testInst);
                    /*System.out.print("For " + pathway + " and " + testP.getUniprot());
                     System.out.print(", actual: " + testInst.classAttribute().value((int) testInst.classValue()));
                     System.out.println(", predicted: " + testInst.classAttribute().value((int) pred));*/
                    if (dist[POSITIVE] > dist[NEGATIVE]) {
                        predP.getPathwayList().add(pathway);
                        //System.out.println(pathway + " Added to predict list");
                    }
                    predictions.addElement(new NominalPrediction(POSITIVE, dist));
                }
                predictList.add(predP);
                pb.setValue((int) ((100.0 * i) / TEST_SIZE));
            }
            return predictList;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public FastVector getPredictions() {
        return predictions;
    }

}
