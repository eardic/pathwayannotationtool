
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.JProgressBar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Emre
 */
public class Correlation extends Algorithm {

    public enum Measure {

        SUPPORT, COSINE, CONFIDENCE, JACCARD, H
    }

    // Correlation and frequency matrices, these will be calculated using protein data set below
    private Map<String, Map<String, Double>> corrMatrix;
    private Map<String, Map<String, Integer>> freqMatrix;
    // Train protein data set
    private Map<Protein, Protein> proteinDataMap;
    // K-Top elements will be added to predict set
    private int k = 5;
    // Default correlation measure
    private Measure measure = Measure.SUPPORT;
    // Progress bar for UI
    private JProgressBar pb = new JProgressBar(0, 100);

    public Correlation(List<PPI> ppiList, List<Protein> testProteinList, List<Protein> trainProteinList, int k) {
        this(ppiList, testProteinList, trainProteinList, k, Measure.SUPPORT, true);
    }

    public Correlation(List<PPI> ppiList, List<Protein> testProteinList, List<Protein> trainProteinList, int k, Measure m) {
        this(ppiList, testProteinList, trainProteinList, k, m, true);
    }

    public Correlation(List<PPI> ppiList, List<Protein> testProteinList, List<Protein> trainProteinList, int k, Measure m, boolean interactionBased) {
        super(ppiList, testProteinList, trainProteinList);
        this.corrMatrix = new HashMap<String, Map<String, Double>>();
        this.freqMatrix = new HashMap<String, Map<String, Integer>>();
        this.proteinDataMap = new HashMap<Protein, Protein>();
        this.measure = m;
        this.k = k;
        initialize();
        if (interactionBased) {
            createFreqByInteraction();
        } else {
            createFreqByPathway();
        }
        //buildNullValues();
        createCorrMatrix();
    }

    public void setPb(JProgressBar pb) {
        this.pb = pb;
    }

    public JProgressBar getPb() {
        return pb;
    }

    private void initialize() {
        Set<String> pathways = new HashSet<String>();
        // Prepare pathway set for corr and freq matrix
        // Build protein dataset
        for (Protein testP : this.trainProteinList) {
            pathways.addAll(testP.getPathwayList());
            proteinDataMap.put(testP, testP);
        }
        for (Protein testP : this.testProteinList) {
            pathways.addAll(testP.getPathwayList());
            proteinDataMap.put(testP, testP);
        }
        // Build freq and corr matrices
        for (String p1 : pathways) {
            freqMatrix.put(p1, new HashMap<String, Integer>());
            corrMatrix.put(p1, new HashMap<String, Double>());
            for (String p2 : pathways) {
                freqMatrix.get(p1).put(p2, 0);
                corrMatrix.get(p1).put(p2, -1.0);
            }
        }
        // Special null row/column
        /*freqMatrix.put("null", new HashMap<String, Integer>());
         corrMatrix.put("null", new HashMap<String, Double>());
         for (String p : pathways) {
         freqMatrix.get("null").put(p, 0);
         corrMatrix.get("null").put(p, -1.0);
         freqMatrix.get(p).put("null", 0);
         corrMatrix.get(p).put("null", -1.0);
         }
         freqMatrix.get("null").put("null", 0);
         corrMatrix.get("null").put("null", -1.0);*/

        System.out.println("CM Initialized!");
    }

    private double cosineMeasure(String pathway1, String pathway2) {
        double fplus1 = 0, f1plus = 0, f11;
        f11 = freqMatrix.get(pathway1).get(pathway2);
        for (String p1 : freqMatrix.keySet()) {
            fplus1 += freqMatrix.get(pathway1).get(p1);//[pathway1][p1]
            f1plus += freqMatrix.get(p1).get(pathway2);//[p1][pathway2]
        }
        return f11 / (f1plus * fplus1);
    }

    private double confidenceMeasure(String pathway1, String pathway2) {
        double fplus1 = 0, f1plus = 0, f11;
        f11 = freqMatrix.get(pathway1).get(pathway2);
        for (String p1 : freqMatrix.keySet()) {
            fplus1 += freqMatrix.get(pathway1).get(p1);//[pathway1][p1]
            f1plus += freqMatrix.get(p1).get(pathway2);//[p1][pathway2]
        }
        return Math.max(f11 / f1plus, f11 / fplus1);
    }

    private double jaccardMeasure(String pathway1, String pathway2) {
        double fplus1 = 0, f1plus = 0, f11, f10, f01;
        f11 = freqMatrix.get(pathway1).get(pathway2);
        for (String p1 : freqMatrix.keySet()) {
            fplus1 += freqMatrix.get(pathway1).get(p1);//[pathway1][p1]
            f1plus += freqMatrix.get(p1).get(pathway2);//[p1][pathway2]
        }
        f10 = f1plus - f11;
        f01 = fplus1 - f11;
        return f11 / (f11 + f10 + f01);
    }

    private double hMeasure(String pathway1, String pathway2) {
        double fplus1 = 0, f1plus = 0, f11, f10, f01;
        f11 = freqMatrix.get(pathway1).get(pathway2);
        for (String p1 : freqMatrix.keySet()) {
            fplus1 += freqMatrix.get(pathway1).get(p1);//[pathway1][p1]
            f1plus += freqMatrix.get(p1).get(pathway2);//[p1][pathway2]
        }
        f10 = f1plus - f11;
        f01 = fplus1 - f11;
        return 1 - ((f10 * f01) / (fplus1 * f1plus));
    }

    private double supportMeasure(String pathway1, String pathway2) {
        return freqMatrix.get(pathway1).get(pathway2);
    }

    private void createFreqByPathway() {
        for (Protein p : proteinDataMap.keySet()) {
            int size = p.getPathwayList().size();
            Object paths[] = p.getPathwayList().toArray();
            for (int i = 0; i < size; ++i) {
                for (int j = i + 1; j < size; ++j) {
                    int pre = freqMatrix.get(paths[i]).get(paths[j].toString());
                    freqMatrix.get(paths[i]).put(paths[j].toString(), pre + 1);
                    freqMatrix.get(paths[j]).put(paths[i].toString(), pre + 1);
                }
                int pre = freqMatrix.get(paths[i]).get(paths[i].toString());
                freqMatrix.get(paths[i]).put(paths[i].toString(), pre + 1);
            }
        }
        System.out.println("Created Frequency Matrix By Annotation.");
    }

    private void buildNullValues() {
        for (Protein p : proteinDataMap.keySet()) {
            for (String path : p.getPathwayList()) {
                Map<String, Integer> nullRow = freqMatrix.get("null");
                Map<String, Integer> nullCol = freqMatrix.get(path);
                nullRow.put(path, nullRow.get(path) + 1);
                nullCol.put("null", nullCol.get("null") + 1);
            }
        }
    }

    private void createFreqByInteraction() {
        try {
            for (PPI ppi : this.ppiList) {
                Set<String> pList1 = ppi.p1.getPathwayList();
                Set<String> pList2 = ppi.p2.getPathwayList();
                for (String p1 : pList1) {
                    // Fill table for p1 and p2
                    for (String p2 : pList2) {
                        if (!p1.equals(p2)) {
                            freqMatrix.get(p1).put(p2, freqMatrix.get(p1).get(p2) + 1);
                            freqMatrix.get(p2).put(p1, freqMatrix.get(p2).get(p1) + 1);
                        }
                    }
                    freqMatrix.get(p1).put(p1, freqMatrix.get(p1).get(p1) + 1);
                }
                for (String p2 : pList2) {
                    freqMatrix.get(p2).put(p2, freqMatrix.get(p2).get(p2) + 1);
                }
            }
            System.out.println("Created Frequency Matrix By Interaction.");
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
    }

    private double calculateMeasure(String p1, String p2) {
        double val = 0;
        switch (this.measure) {
            case SUPPORT:
                val = supportMeasure(p1, p2);
                break;
            case COSINE:
                val = cosineMeasure(p1, p2);
                break;
            case CONFIDENCE:
                val = confidenceMeasure(p1, p2);
                break;
            case H:
                val = hMeasure(p1, p2);
                break;
            case JACCARD:
                val = jaccardMeasure(p1, p2);
                break;
        }
        return val;
    }

    private void createCorrMatrix() {
        for (String p1 : corrMatrix.keySet()) {
            for (String p2 : corrMatrix.keySet()) {
                if (corrMatrix.get(p2).get(p1) > -1.0) {
                    corrMatrix.get(p1).put(p2, corrMatrix.get(p2).get(p1));
                } else if (corrMatrix.get(p1).get(p2) > -1.0) {
                    corrMatrix.get(p2).put(p1, corrMatrix.get(p1).get(p2));
                } else {
                    double val = calculateMeasure(p1, p2);
                    corrMatrix.get(p1).put(p2, val);
                    corrMatrix.get(p2).put(p1, val);
                }
            }
        }
        System.out.println("Created Correlation Matrix using " + this.measure.name());
    }

    @Override
    public List<Protein> predict() {
        List<Protein> prediction = new LinkedList<Protein>();
        final int testSize = testProteinList.size();
        //debugMatrices();
        for (int t = 0; t < testSize; ++t) {
            Protein testP = testProteinList.get(t);
            pb.setValue((int) ((100.0 * t) / testSize));
            // Set of proteins which interacts with selected protein
            Set<Protein> protSetForTestP = new HashSet<Protein>();
            // Create set of proteins interacting with testP
            for (PPI ppi : ppiList) {
                if (ppi.p1.equals(testP)) {//If testP is p1 then add p2
                    protSetForTestP.add(ppi.p2);
                } else if (ppi.p2.equals(testP)) {//else add p1
                    protSetForTestP.add(ppi.p1);
                }
            }
            if (protSetForTestP.isEmpty()) {
                System.err.println("No neighbor of " + testP);
            }
            // Create pathway set for testP using protein set of testP
            List<String> pathwayListForTestP = new LinkedList<String>();
            for (Protein setP : protSetForTestP) {
                Protein pat = this.proteinDataMap.get(setP);
                if (pat != null) {
                    pathwayListForTestP.addAll(pat.getPathwayList());
                } else {
                    // System.err.println(setP + " not in protein data set");
                }
            }
            if (pathwayListForTestP.isEmpty()) {
                System.err.println("No pathway list of " + testP);
            }
            //pathwayListForTestP.add("null");
            // Create correlation vectors for pathway set of testP
            List<Map<String, Double>> corrVectors = new LinkedList<Map<String, Double>>();
            for (String pathway : pathwayListForTestP) {
                Map<String, Double> corrCol = new HashMap<String, Double>();
                for (String corrP : corrMatrix.keySet()) {
                    //if (!"null".equals(corrP)) {
                    corrCol.put(corrP, corrMatrix.get(corrP).get(pathway));
                    // }
                }
                corrVectors.add(corrCol);
            }
            // Sum all correlation vectors
            Map<String, Double> summedCorrVector = new HashMap<String, Double>();
            for (Map<String, Double> vec : corrVectors) {
                for (String corrP : vec.keySet()) {
                    if (summedCorrVector.containsKey(corrP)) {
                        double pre = summedCorrVector.get(corrP);
                        summedCorrVector.put(corrP, pre + vec.get(corrP));
                    } else {//initialization at first run
                        summedCorrVector.put(corrP, vec.get(corrP));
                    }
                }
            }
            // Normalization
            // Find max
            double max = Double.MIN_VALUE;
            for (String pathway : summedCorrVector.keySet()) {
                if (summedCorrVector.get(pathway) > max) {
                    max = summedCorrVector.get(pathway);
                }
            }
            // Normalize
            for (String pathway : summedCorrVector.keySet()) {
                summedCorrVector.put(pathway, summedCorrVector.get(pathway) / max);
            }
            if (summedCorrVector.isEmpty()) {
                //System.err.println("No summed vector for " + testP);
            }
            // Create predicted pathway list for testP
            Protein predictedProtein = new Protein(testP.getUniprot());
            // Sort map by value, from the highest to the lowest
            List<Entry<String, Double>> list = MapUtil.sortByValue(summedCorrVector);
            // Get biggest k element in map by value and create pathway list for testP
            Set<String> pathwayList = predictedProtein.getPathwayList();
            for (int i = 0; i < this.k && i < list.size(); ++i) {
                String key = list.get(i).getKey();
                pathwayList.add(key);
            }
            prediction.add(predictedProtein);
            //debugPrint(summedCorrVector, pathwayListForTestP, protSetForTestP, list, testP);
        }// END FOR
        pb.setValue(pb.getMaximum());
        return prediction;
    }

    private void debugMatrices() {

        System.err.println("Freq Matrix");
        System.err.print("\t");
        for (String p1 : freqMatrix.keySet()) {
            System.err.print(p1 + "\t");
        }
        System.err.println("");
        for (String p1 : freqMatrix.keySet()) {
            if ("null".equals(p1)) {
                System.err.print(p1 + "  [");
            } else {
                System.err.print(p1 + "\t[");
            }
            for (String p2 : freqMatrix.keySet()) {
                System.err.print(freqMatrix.get(p1).get(p2) + "\t");
            }
            System.err.println("]");
        }

        System.err.println("Correlation Matrix");
        System.err.print("\t");
        for (String p1 : corrMatrix.keySet()) {
            System.err.print(p1 + "\t");
        }
        System.err.println("");
        for (String p1 : corrMatrix.keySet()) {
            if ("null".equals(p1)) {
                System.err.print(p1 + "  [");
            } else {
                System.err.print(p1 + "\t[");
            }
            for (String p2 : corrMatrix.keySet()) {
                System.err.print(corrMatrix.get(p1).get(p2) + "\t");
            }
            System.err.println("]");
        }
    }

    private void debugPrint(Map<String, Double> summedCorrVector, List<String> pathwayListForTestP,
            Set<Protein> protSetForTestP, List<Entry<String, Double>> list, Protein testP) {

        System.err.println("\nProtein Set for " + testP);
        System.err.print("[");
        for (Protein setP : protSetForTestP) {
            System.err.print(setP.getUniprot() + ",");
        }
        System.err.println("]");

        System.err.println("Pathway Set for " + testP);
        System.err.print("[");
        for (String p : pathwayListForTestP) {
            System.err.print(p + ",");
        }
        System.err.println("]");

        System.err.println("Summed Correlation Vector");
        System.err.print("[");
        for (String key : summedCorrVector.keySet()) {
            System.err.print(summedCorrVector.get(key) + ",");
        }
        System.err.println("]");

        System.err.println("Sorted Correlation Vector");
        System.err.print("[");
        for (int i = 0; i < list.size(); ++i) {
            System.err.print(list.get(i) + ",");
        }
        System.err.println("]\n");
    }

}
