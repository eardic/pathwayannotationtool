
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ASUS-PC
 */
public class Protein implements Comparable<Protein> {

    private Set<String> pathwayList;
    private String uniprot;

    public Protein() {
        this("");
    }

    public Protein(String uniprot) {
        this.pathwayList = new HashSet<String>();
        this.uniprot = uniprot;
    }

    public Protein(Protein p) {
        uniprot = p.getUniprot();
        pathwayList = new HashSet<String>(p.getPathwayList());
    }

    public void setPathwayList(Set<String> pathwayList) {
        this.pathwayList = pathwayList;
    }

    public Set<String> getPathwayList() {
        return pathwayList;
    }

    public String getUniprot() {
        return uniprot;
    }

    public void setUniprot(String uniprot) {
        this.uniprot = uniprot;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == this.hashCode();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.uniprot);
        return hash;
    }

    @Override
    public int compareTo(Protein o) {
        return o.getUniprot().compareTo(this.uniprot);
    }

    @Override
    public String toString() {
        return this.uniprot + "," + pathwayList.size();
    }

}
